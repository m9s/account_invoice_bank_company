#This file is part of Tryton. The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
{
    'name': 'Account Invoice Bank Company',
    'name_de_DE': 'Fakturierung Bankverbindung Unternehmen',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz/',
    'description': '''Account Invoice Bank Company
    - Adds a field bank account to invoices
      to be shown on reports.
''',
    'description_de_DE': '''Fakturierung Bankverbindung Unternehmen
    - Fügt das Feld Bankkonto zu Rechnungen hinzu.
    - Dient der Angabe der Bankverbindung des Unternehmens
      auf den entsprechenden Berichten.
''',
    'depends': [
        'account_invoice',
        'party_bank',
    ],
    'xml': [
        'invoice.xml'
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}

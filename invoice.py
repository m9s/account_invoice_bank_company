#This file is part of Tryton. The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Not, Equal, Eval
from trytond.transaction import Transaction
from trytond.pool import Pool


class Invoice(ModelSQL, ModelView):
    _name = 'account.invoice'

    bank_account = fields.Many2One('bank.account', 'Bank Account', domain=[
                ('company_account','=', True),
            ], context={'company': Eval('company')}, required=True, states={
                'readonly': Not(Equal(Eval('state'), 'draft')),
            }, depends=['state'])

    def default_bank_account(self):
        company_obj = Pool().get('company.company')
        if Transaction().context.get('company'):
            company = company_obj.browse(Transaction().context['company'])
            if company.bank_accounts:
                return company.bank_accounts[0].id
        return {}

Invoice()


class BankAccount(ModelSQL, ModelView):
    _name = 'bank.account'

    # This is a dummy function field for catching the search domain
    # and rewrite it with a real search domain.
    company_account = fields.Function(fields.Boolean('Company Account'),
            None, # No getter required
            searcher='search_company_party')

    def __init__(self):
        super(BankAccount, self).__init__()
        self._error_messages.update({
                'create_company': 'The current user is not associated ' \
                        'to a company. Please create a company and associate ' \
                        'the user to it.'})

    def search_company_party(self, name, clause):
        company_obj = Pool().get('company.company')
        res = []
        company = company_obj.browse(Transaction().context.get('company', 0))
        if company:
            res = [('party.id', '=', company.party.id)]
        return res

    def default_party(self):
        company_obj = Pool().get('company.company')
        if Transaction().context.get('company'):
            company = company_obj.browse(Transaction().context['company'])
            return company.party.id
        else:
            self.raise_user_error('create_company')
            return {}

BankAccount()
